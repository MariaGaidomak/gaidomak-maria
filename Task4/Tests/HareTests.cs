﻿using Game;
using NUnit.Framework;

namespace Tests
{
    public class HareTests
    {
        [Test]
        public void AvoidPlayerTest()
        {
            PlayingField field = new PlayingField(6, 6, null);
            Hare hare = new Hare(field, 3, 3);
            field.AddElement(hare);
            field.Player.CoordinateX = 5;
            field.Player.CoordinateX = 5;
            hare.DoStep();
            hare.DoStep();
            hare.DoStep();
            Assert.IsTrue(hare.CoordinateX != 5 || hare.CoordinateY != 5);
            hare.DoStep();
            hare.DoStep();
            hare.DoStep();
            Assert.IsTrue(hare.CoordinateX != 5 || hare.CoordinateY != 5);
        }

        [Test]
        public void SetBonusesTest()
        {
            PlayingField field = new PlayingField(4, 4, null);
            Hare hare = new Hare(field, 0, 1);
            hare.Probability = 1;
            hare.DoStep();
            Assert.IsNotNull(field.GetElement(0, 1));
        }
    }
}
