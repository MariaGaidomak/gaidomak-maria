﻿using Game;
using NUnit.Framework;
namespace Tests
{
    public class HamsterTests
    {

        [Test]
        public void EatBlueberryTest()
        {
            PlayingField field = new PlayingField(1, 2, null);
            Hamster hamster = new Hamster(field, 1, 0);
            field.AddElement(hamster);
            Blueberry blueberry = new Blueberry(0, 0);
            field.AddElement(blueberry);
            System.Console.WriteLine($"X={hamster.CoordinateX} Y={hamster.CoordinateY}");
            hamster.DoStep();
            System.Console.WriteLine($"X={hamster.CoordinateX} Y={hamster.CoordinateY}");
            Assert.AreEqual(7, hamster.GetState().NumberOfPassesThroughObstacles);
        }

        [Test]
        public void InteractWithPlayerTest()
        {
            PlayingField field = new PlayingField(5, 5, null);
            Hamster hamster = new Hamster(field, 1, 0);
            field.AddElement(hamster);
            Assert.AreEqual(100, field.Player.GetState().Health);
            hamster.DoStep();
            Assert.AreEqual(85, field.Player.GetState().Health);
        }

        [Test]
        public void ExplosionBreakObstaclesTest()
        {
            PlayingField field = new PlayingField(5, 5, null);
            Hamster hamster = new Hamster(field, 1, 0);
            field.AddElement(hamster);
            Stone stone1 = new Stone(2, 0);
            field.AddElement(stone1);
            Stone stone2 = new Stone(3,0);
            field.AddElement(stone2);
            Stone stone3 = new Stone(0, 2);
            field.AddElement(stone3);
            Stone stone4 = new Stone(0, 3);
            field.AddElement(stone4);
            Assert.AreEqual(100, field.Player.GetState().Health);
            hamster.DoStep();
            Assert.AreEqual(85, field.Player.GetState().Health);
            Assert.IsNull(field.GetElement(2, 0));
            Assert.IsNull(field.GetElement(3, 0));
            Assert.IsNull(field.GetElement(0, 2));
            Assert.IsNull(field.GetElement(0, 3));
        }
        [Test]
        public void ExplosionBreakBonusesTest()
        {
            PlayingField field = new PlayingField(5, 5, null);
            Hamster hamster = new Hamster(field, 1, 0);
            field.AddElement(hamster);
            Apple apple1 = new Apple(2, 0);
            field.AddElement(apple1);
            Apple apple2 = new Apple(3, 0);
            field.AddElement(apple2);
            Apple apple3 = new Apple(0, 2);
            field.AddElement(apple3);
            Apple apple4 = new Apple(0, 3);
            field.AddElement(apple4);
            Assert.AreEqual(100, field.Player.GetState().Health);
            hamster.DoStep();
            Assert.AreEqual(85, field.Player.GetState().Health);
            Assert.IsNull(field.GetElement(2, 0));
            Assert.IsNull(field.GetElement(3, 0));
            Assert.IsNull(field.GetElement(0, 2));
            Assert.IsNull(field.GetElement(0, 3));
        }

        [Test]
        public void ExplosionKillMonstersTest()
        {
            PlayingField field = new PlayingField(5, 5, null);
            Hamster hamster = new Hamster(field, 1, 0);
            field.AddElement(hamster);
            Wolf wolf = new Wolf(field, 2, 0);
            field.AddElement(wolf);
            Bear bear = new Bear(field, 0, 2);
            field.AddElement(bear);
            Assert.AreEqual(100, field.Player.GetState().Health);
            hamster.DoStep();
            Assert.AreEqual(85, field.Player.GetState().Health);
            Assert.IsNull(field.GetElement(2, 0));
            Assert.IsNull(field.GetElement(0, 2));
        }

        [Test]
        public void FollowPlayerTest()
        {
            PlayingField field = new PlayingField(5, 5, null);
            Hamster hamster = new Hamster(field, 1, 0);
            field.AddElement(hamster);
            field.Player.CoordinateX = 4;
            field.Player.CoordinateY = 4;
            hamster.DoStep();
            hamster.DoStep();
            hamster.DoStep();
            hamster.DoStep();
            Assert.AreEqual(hamster.CoordinateX, 4);
            Assert.AreEqual(hamster.CoordinateY, 4);
        }
    }
}
