﻿using Game;
using Game.DTOs;
using NUnit.Framework;

namespace Tests
{
    public class FactoryElementTests
    {
        [Test]
        public void CreateAppleTest()
        {
            PlayingField field = new PlayingField(10, 10, null);
            FactoryElement factory = new FactoryElement(field);
            PlayingElement apple = factory.CreateElement(Types.Apple);
            Assert.IsInstanceOf<Apple>(apple);
        }

        [Test]
        public void CreateFoxTest()
        {
            PlayingField field = new PlayingField(10, 10, null);
            FactoryElement factory = new FactoryElement(field);
            PlayingElement fox = factory.CreateElement(Types.Fox);
            Assert.IsInstanceOf<Fox>(fox);
        }

        [Test]
        public void CreateStoneFromStateTest()
        {
            PlayingField field = new PlayingField(10, 10, null);
            FactoryElement factory = new FactoryElement(field);
            ElementState state = new ElementState(Types.Stone, 1, 2);
            PlayingElement stone = factory.CreateElement(state);
            Assert.IsInstanceOf<Stone>(stone);
            Assert.AreEqual(1, stone.CoordinateX);
            Assert.AreEqual(2, stone.CoordinateY);
        }

        [Test]
        public void CreateWolfFromStateTest()
        {
            PlayingField field = new PlayingField(10, 10, null);
            FactoryElement factory = new FactoryElement(field);
            ElementState state = new ElementState(Types.Wolf, 1, 2)
            {
                ImpactOnHelth = -11,
            };
            PlayingElement wolf = factory.CreateElement(state);
            Assert.IsInstanceOf<Wolf>(wolf);
            Assert.AreEqual(1, wolf.CoordinateX);
            Assert.AreEqual(2, wolf.CoordinateY);
        }
    }
}
