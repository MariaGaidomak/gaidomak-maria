﻿using Game;
using NUnit.Framework;
namespace Tests
{
    public class BearTests
    {
        [Test]
        public void EatRaspberryTest()
        {
            PlayingField field = new PlayingField(1, 2, null);
            Bear bear = new Bear(field, 0, 0);
            field.AddElement(bear);
            Raspberries raspberry = new Raspberries(1, 0);
            field.AddElement(raspberry);
            Assert.AreEqual(-30, bear.ImpactOnHelth);
            bear.DoStep();
            Assert.AreEqual(-60, bear.ImpactOnHelth);
            Assert.AreEqual(bear, field.GetElement(1, 0));
            Assert.IsFalse(field.HasBonuses());
        }

        [Test]
        public void EatBlueberryTest()
        {
            PlayingField field = new PlayingField(1, 2, null);
            Bear bear = new Bear(field, 0, 0);
            field.AddElement(bear);
            Blueberry blueberry = new Blueberry(1, 0);
            field.AddElement(blueberry);
            Assert.AreEqual(0, bear.GetState().NumberOfPassesThroughObstacles);
            bear.DoStep();
            Assert.AreEqual(7, bear.GetState().NumberOfPassesThroughObstacles);
            Stone stone = new Stone(0, 0);
            field.AddElement(stone);
            bear.DoStep();
            Assert.AreEqual(6, bear.GetState().NumberOfPassesThroughObstacles);
        }

        [Test]
        public void InteractWithPlayer()
        {
            PlayingField field = new PlayingField(1, 2, null);
            Bear bear = new Bear(field, 1, 0);
            field.AddElement(bear);
            Assert.AreEqual(100, field.Player.GetState().Health);
            bear.DoStep();
            Assert.AreEqual(70, field.Player.GetState().Health);
            bear.DoStep();
            Assert.AreEqual(70, field.Player.GetState().Health);
            bear.DoStep();
            Assert.AreEqual(40, field.Player.GetState().Health);
        }
    }
}
