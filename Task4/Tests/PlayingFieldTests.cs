﻿using Game;
using Game.DTOs;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;

namespace Tests
{
    public class PlayingFieldTests
    {
        [Test]
        public void SizeTest()
        {
            PlayingField field = new PlayingField(10, 15, null);
            Assert.AreEqual(15, field.FieldWidth);
            Assert.AreEqual(10, field.FieldНeight);
        }

        [Test]
        public void CreatePlayerTest()
        {
            PlayingField field = new PlayingField(10, 15, null);
            Assert.IsNotNull(field.Player);
        }

        [Test]
        public void AddAndGetElementTest()
        {
            PlayingField field = new PlayingField(10, 15, null);
            Stone stone = new Stone(1, 2);
            field.AddElement(stone);
            PlayingElement fromField = field.GetElement(1, 2);
            Assert.AreEqual(stone, fromField);
        }

        [Test]
        public void HasBonusesTest()
        {
            PlayingField field = new PlayingField(10, 15, null);
            Stone stone = new Stone(1, 2);
            field.AddElement(stone);
            Assert.IsFalse(field.HasBonuses());
            Raspberries raspberry = new Raspberries(2, 3);
            field.AddElement(raspberry);
            Assert.IsTrue(field.HasBonuses());
        }

        [Test]
        public void DeleteElementTest()
        {
            PlayingField field = new PlayingField(10, 15, null);
            Raspberries raspberry = new Raspberries(2, 3);
            field.AddElement(raspberry);
            Assert.IsNotNull(field.GetElement(2, 3));
            field.DeleteElement(raspberry);
            Assert.IsNull(field.GetElement(2, 3));
            Assert.IsFalse(field.HasBonuses());
        }

        [Test]
        public void GetActors()
        {
            PlayingField field = new PlayingField(10, 15, null);
            Stone stone = new Stone(1, 2);
            field.AddElement(stone);
            Raspberries raspberry = new Raspberries(2, 3);
            field.AddElement(raspberry);
            IEnumerable<IActor> actors = field.GetActors();
            Assert.AreEqual(1, actors.Count());
            Assert.AreEqual(field.Player, actors.First());
            Fox fox = new Fox(field, 1, 2);
            field.AddElement(fox);
            actors = field.GetActors();
            Assert.AreEqual(2, actors.Count());
            Assert.AreEqual(field.Player, actors.First());
            Assert.AreEqual(fox, actors.Last());
        }

        [Test]
        public void GetStateTest()
        {
            PlayingField field = new PlayingField(10, 15, null);
            Raspberries raspberry = new Raspberries(2, 3);
            field.AddElement(raspberry);
            Stone stone = new Stone(1, 2);
            field.AddElement(stone);
            GameState state = field.GetState();
            Assert.NotNull(state);
            Assert.AreEqual(field.FieldWidth, state.FieldWidth);
            Assert.AreEqual(field.FieldНeight, state.FieldLength);
            Assert.AreEqual(field.Player.CoordinateX, state.Player.X);
            Assert.AreEqual(field.Player.CoordinateY, state.Player.Y);
            Assert.AreEqual(2, state.ElementsState.Count);
        }
    }
}
