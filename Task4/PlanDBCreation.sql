create database GameDB;
go

use GameDB;

create table [Level](
Id int not null identity(1,1) primary key,
LevelNumber tinyint not null,
FieldWidth int not null,
FieldHeight int not null
);

create table [LevelData](
Id int not null identity(1,1) primary key,
Type tinyint not null,
Quantity int not null,
LevelId int not null,
foreign key (LevelId) references Level(Id) 
);

create table [GameResult](
Id int not null identity(1,1) primary key,
PlayerName nvarchar(50) not null,
StepsQuantity int not null,
GameDate datetime2 not null,
LevelId int not null,
foreign key (LevelId) references Level(Id) 
);

create table [State](
Id int not null identity(1,1) primary key,
LastSaveDate datetime2 not null,
LevelId int not null,
foreign key (LevelId) references Level(Id) 
);

create table [Player](
Id int not null identity(1,1) primary key,
X int not null,
Y int not null,
Health tinyint not null,
NumberOfPassesThroughObstacles tinyint not null,
StateId int not null,
foreign key (StateId) references State(Id) 
);

create table [Monsters](
Id int not null identity(1,1) primary key,
Type tinyint not null,
X int not null,
Y int not null,
ImpactOnHealth smallint not null,
NumberOfPassesThroughObstacles tinyint not null,
StateId int not null,
foreign key (StateId) references State(Id) 
);

create table [Obstacles](
Id int not null identity(1,1) primary key,
Type tinyint not null,
X int not null,
Y int not null,
StateId int not null,
foreign key (StateId) references State(Id) 
);

create table [Bonuses](
Id int not null identity(1,1) primary key,
Type tinyint not null,
X int not null,
Y int not null,
StateId int not null,
foreign key (StateId) references State(Id) 
);

