﻿using System.Collections.Generic;
using Game.DTOs;
using System.Linq;

namespace Game
{
    public class PlayingField
    {
        private Player _player;
        public Player Player 
        {
            get
            {
                return _player;
            }
        }
        public int FieldНeight { get; }
        public int FieldWidth { get; }
        private List<PlayingElement> _elements;
        public PlayingField(int length, int width, UserInputProcessor inputProcessor)
        {
            FieldНeight = length;
            FieldWidth = width;
            _player = new Player(this, inputProcessor, 0, 0); ;
            _elements = new List<PlayingElement>();
        }
        public PlayingField(GameState state, UserInputProcessor inputProcessor)
        {
            FieldНeight = state.FieldLength;
            FieldWidth = state.FieldWidth;
            _player = new Player(this, state.Player, inputProcessor); 
            _elements = new List<PlayingElement>();
        }
        public PlayingElement GetElement(int x, int y)
        {
            return _elements.Find(e => e.CoordinateX == x && e.CoordinateY == y);
        }
        public void DeleteElement(PlayingElement element)
        {
            _elements.Remove(element);
        }
        public void AddElement(PlayingElement element)
        {
            _elements.Add(element);
        }
        public bool HasBonuses()
        {
            foreach (var item in _elements)
            {
                if (item is IBonus)
                {
                    return true;
                }
            }
            return false;
        }
        public IEnumerable<IActor> GetActors()
        {
            List<IActor> actors = new List<IActor>();
            actors.Add(_player);
            foreach (var item in _elements)
            {
                if (item is IActor actor)
                {
                    actors.Add(actor);
                }
            }
            return actors;
        }
        public GameState GetState()
        {
            GameState state = new GameState(FieldWidth, FieldНeight);
            state.ElementsState = _elements.Select(x => x.GetState()).ToList();
            state.Player = Player.GetState();
            return state;
        }
    }
}
