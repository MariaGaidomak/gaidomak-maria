﻿namespace Game
{
    public enum Types
	{
        Bear = 1,
        Wolf,
	    Fox,
	    Bush,
	    Stone,
	    Wall,
	    Apple,
	    Pineapple,
	    Blueberry,
	    Raspberries,
		Player,
		Hamster,
		Hare,
	}
}
