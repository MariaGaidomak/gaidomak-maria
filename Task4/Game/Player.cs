﻿using System;
using Game.DTOs;

namespace Game
{
    public class Player : PlayingElement, IActor
    {
        private PlayingField _field;
        private int _health = 100;
        private int _numberOfPassesThroughObstacles;
        private UserInputProcessor _inputProcessor;

        public int Health 
        { 
            get { return _health; } 
        }
        public Player(PlayingField field, UserInputProcessor inputProcessor, int x, int y)
           : base(x, y)
        {
            Name = "Player";
            _field = field;
            _inputProcessor = inputProcessor;
        }
        public Player(PlayingField field, ElementState state, UserInputProcessor inputProcessor)
            :base(state)
        {
            Name = "Player";
            _field = field;
            _inputProcessor = inputProcessor;
            _health = state.Health;
            _numberOfPassesThroughObstacles = state.NumberOfPassesThroughObstacles;
        }
        public bool IsAlive()
        {
            return _health > 0;
        }
        public void DoStep()
        {
            (int newX, int newY) = GetNewCoordinates();
            PlayingElement element = _field.GetElement(newX, newY);
            if (InteractWithObstacle(element, newX, newY))
            {
                DoStep();
                return;
            }
            Blueberry blueberry =  element as Blueberry;
            if (blueberry != null)
            {
                _numberOfPassesThroughObstacles = 7;
                _field.DeleteElement(blueberry);
            }
    
            InteractWithEntitiesAffectingHealth(element, newX, newY);
            CoordinateX = newX;
            CoordinateY = newY;
       
        }
        private (int x, int y) GetNewCoordinates()
        {
            ConsoleKeyInfo info = _inputProcessor.ReadKey();
            var newX = CoordinateX;
            var newY = CoordinateY;
            switch (info.Key)
            {

                case ConsoleKey.LeftArrow:
                    newX--;
                    break;
                case ConsoleKey.RightArrow:
                    newX++;
                    break;
                case ConsoleKey.UpArrow:
                    newY--;
                    break;
                case ConsoleKey.DownArrow:
                    newY++;
                    break;
            }
            if (newX == -1 || newY == -1 || newX == _field.FieldWidth || newY == _field.FieldНeight)
            {
                return GetNewCoordinates();
            }

            return (newX, newY);
        }
        private bool InteractWithObstacle(PlayingElement element, int newX, int newY)
        {
            ISurmountabilityBlock surmountability = element as ISurmountabilityBlock;
            if (surmountability != null && !surmountability.SurmountabilityBlock(this))
            {
                if (_numberOfPassesThroughObstacles == 0)
                {
                    return true;
                }
                _numberOfPassesThroughObstacles--;
            }
            return false;
        }
        public void InteractWithEntitiesAffectingHealth(PlayingElement element, int newX, int newY)
        {
            IImpactOnHelth impactOnHelth = element as IImpactOnHelth;
            if (impactOnHelth != null)
            {
                _health += impactOnHelth.ImpactOnHelth;
                if (_health > 100)
                {
                    _health = 100;
                }
                if (impactOnHelth is IBonus)
                {
                    _field.DeleteElement(element);
                }
            }
        }
        public override void Draw()
        {
            Console.ForegroundColor = ConsoleColor.White;
            Console.Write("@");
        }
        public override ElementState GetState()
        {
            ElementState state = base.GetState();
            state.Health = _health;
            state.NumberOfPassesThroughObstacles = _numberOfPassesThroughObstacles;
            return state;
        }
        public override Types GetElementType()
        {
            return Types.Player;
        }
    }
}
