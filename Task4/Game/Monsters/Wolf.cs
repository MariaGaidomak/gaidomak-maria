﻿using System;
using Game.DTOs;


namespace Game
{
    public class Wolf: Monster, IActor, IImpactOnHelth
    {
        public Wolf(PlayingField field, int x, int y)
            : base(field, x, y)
        {
            Name = "Wolf";
        }
        public Wolf(PlayingField field, ElementState state)
            : base(field, state)
        {
            Name = "Wolf";
        }
        public override int ImpactOnHelth { get; } = -20;
        public override void Draw()
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write("W");
        }
        public override Types GetElementType()
        {
            return Types.Wolf;
        }
    }
}
