﻿using Game.DTOs;
using System;
using System.Collections.Generic;
using System.IO;

namespace Game 
{
    public class Hamster : Monster, IActor, IImpactOnHelth
    {

        private Random _rnd = new Random();
        public Hamster(PlayingField field, int x, int y)
            : base(field, x, y)
        {
            Name = "Hamster";
        }
        public Hamster(PlayingField field, ElementState state)
           : base(field, state)
        {
            Name = "Hamster";
        }

        public override int ImpactOnHelth { get; } = -15;

        public override void Draw()
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write("H");
        }

        public override Types GetElementType()
        {
            return Types.Hamster;
        }

        public void Explode()
        {
            for (int i = -3; i <= 3; i++)
            {
                for (int j = -3; j <= 3; j++)
                {
                    var element = _field.GetElement(CoordinateX + i, CoordinateY + j);
                    if (element is not Player)
                        _field.DeleteElement(element);
                }
            }
        }

        public override void DoStep()
        {
            int newX = CoordinateX;
            int newY = CoordinateY;

            if (Math.Abs(_field.Player.CoordinateX - CoordinateX) <= 5 && Math.Abs(_field.Player.CoordinateY - CoordinateY) <= 5)
            {
                if (_field.Player.CoordinateX < CoordinateX && CoordinateX - 1 >= 0)
                    newX = CoordinateX-1;
                if (_field.Player.CoordinateX > CoordinateX && CoordinateX + 1 < _field.FieldWidth)
                    newX = CoordinateX+1;
                if (_field.Player.CoordinateY < CoordinateY && CoordinateY - 1 >= 0)
                    newY = CoordinateY-1;
                if (_field.Player.CoordinateY > CoordinateY && CoordinateY + 1 < _field.FieldНeight)
                    newY = CoordinateY+1;
            }
            else
                base.GenerateCoordinates(out newX, out newY);
        
            if (base.InteractWithObstacle(newX, newY))
            {
                base.DoStep();
                return;
            }
            InteractWithBonuses(newX, newY);
            SetCoordinates(newX, newY);
            InteractWithPlayer(CoordinateX, CoordinateY);
        }

        protected override void InteractWithPlayer(int newX, int newY)
        {
            if (newX == _field.Player.CoordinateX && newY == _field.Player.CoordinateY)
            {
                _field.Player.InteractWithEntitiesAffectingHealth(this, newX, newY);
                Explode();
            }
        }
    }
 }