﻿using System;
using Game.DTOs;

namespace Game
{
    public abstract class Monster: PlayingElement, IImpactOnHelth, IActor
    {
        protected PlayingField _field;
        private Random _rnd = new Random();
        private int _numberOfPassesThroughObstacles;
        public Monster(PlayingField field, int x, int y)
            :base(x, y)
        {
            _field = field;
        }
        public Monster(PlayingField field, ElementState state)
            :base(state)
        {
            _field = field;
            _numberOfPassesThroughObstacles = state.NumberOfPassesThroughObstacles;
        }
        public abstract int ImpactOnHelth { get; }
        public virtual void DoStep()
        {
            int newX;
            int newY;
            GenerateCoordinates(out newX, out newY);
            if (InteractWithObstacle(newX, newY))
            {
                DoStep();
                return;
            }
            InteractWithBonuses(newX, newY);
            SetCoordinates(newX, newY);
            InteractWithPlayer(newX, newY);
        }
        protected virtual void InteractWithPlayer(int newX, int newY)
        {
            if (newX == _field.Player.CoordinateX && newY == _field.Player.CoordinateY)
            {
                _field.Player.InteractWithEntitiesAffectingHealth(this, newX, newY);
            }       
        }
        protected virtual void InteractWithBonuses(int newX, int newY)
        {
            Blueberry blueberry = _field.GetElement(newX,newY) as Blueberry;
            if (blueberry != null)
            {
                _numberOfPassesThroughObstacles = 7;
                _field.DeleteElement(blueberry);
            }
        }
        protected virtual bool InteractWithObstacle(int newX, int newY)
        {
            ISurmountabilityBlock surmountability = _field.GetElement(newX, newY) as ISurmountabilityBlock;
            if (surmountability != null && !surmountability.SurmountabilityBlock(this))
            {
                if (_numberOfPassesThroughObstacles == 0)
                {
                    return true;
                }
                _numberOfPassesThroughObstacles--;
            }
            return false;
        }
        protected virtual void GenerateCoordinates(out int newX, out int newY)
        {
            int dx = _rnd.Next(3) - 1;
            int dy = _rnd.Next(3) - 1;
            newX = CoordinateX;
            newY = CoordinateY;
            if (CoordinateX + dx >= 0 && CoordinateX + dx < _field.FieldWidth)
                newX += dx;
            if (CoordinateY + dy >= 0 && CoordinateY + dy < _field.FieldНeight)
                newY += dy;
            if (CoordinateX == newX && CoordinateY == newY)
            {
                GenerateCoordinates(out newX, out newY);
                return;
            }
        }
        protected virtual void SetCoordinates(int newX, int newY)
        {
            CoordinateX = newX;
            CoordinateY = newY;
        }
        public override ElementState GetState()
        {
            ElementState state = base.GetState();
            state.NumberOfPassesThroughObstacles = _numberOfPassesThroughObstacles;
            return state;
        }
    }
}
