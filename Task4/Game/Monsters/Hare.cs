﻿using Game.DTOs;
using System;

namespace Game
{
    public class Hare: Monster, IActor, IImpactOnHelth
    {
        public Hare(PlayingField field, int x, int y)
           : base(field, x, y)
        {
            Name = "Hare";
            Probability = 0.5;
        }
        public Hare(PlayingField field, ElementState state)
           : base(field, state)
        {
            Name = "Hare";
            Probability = 0.9;
        }

        public double Probability { get; set; }
        public override int ImpactOnHelth { get; } = -25;
        private Random probabilityRnd = new Random();

        public override void Draw()
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write("h");
        }

        public override Types GetElementType()
        {
            return Types.Hare;
        }

        public override void DoStep()
        {
            int newX = CoordinateX;
            int newY = CoordinateY;
            if (Math.Abs(_field.Player.CoordinateX - CoordinateX) <= 3 && Math.Abs(_field.Player.CoordinateY - CoordinateY) <= 3)
            {
                if (_field.Player.CoordinateX < CoordinateX && CoordinateX + 1 < _field.FieldWidth)
                    newX = CoordinateX + 1;
                if (_field.Player.CoordinateX > CoordinateX && CoordinateX - 1 >= 0)
                    newX = CoordinateX - 1;
                if (_field.Player.CoordinateY < CoordinateY && CoordinateY + 1 < _field.FieldНeight)
                    newY = CoordinateY + 1;
                if (_field.Player.CoordinateY > CoordinateY && CoordinateY - 1 >= 0)
                    newY = CoordinateY - 1;
            }
            else
                base.GenerateCoordinates(out newX, out newY);

            if (base.InteractWithObstacle(newX, newY))
            {
                base.DoStep();
                return;
            }
            SetBonus();
            InteractWithBonuses(newX, newY);
            SetCoordinates(newX, newY);
            InteractWithPlayer(CoordinateX, CoordinateY);
        }

        public void SetBonus()
        {
            int probabilityValue = 10 - (int)(Probability * 10);
            int randomResult = probabilityRnd.Next(0, probabilityValue);
            if (randomResult == 0)
                if (_field.GetElement(CoordinateX, CoordinateY) == null || _field.GetElement(CoordinateX, CoordinateY) == this)
                    _field.AddElement(new Apple(CoordinateX, CoordinateY));
        }
    }
}
