﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game
{
    public class GameResult
    {
        public string  PlayerName { get; set; }
        public int StepsQuantity { get; set; }
        public DateTime GameDate { get; set; }
        public int LevelId { get; set; }
        public int LevelNumber { get; set; }
    }
}
