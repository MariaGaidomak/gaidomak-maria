﻿using System.Collections.Generic;


namespace Game.DTOs
{
    public class GameState
    {
        public int FieldWidth { get; set; }
        public int FieldLength { get; set; }
        public List<ElementState> ElementsState { get; set; }
        public ElementState Player { get; set; }
        public int LevelId { get; set; }
        public GameState()
        {
            ElementsState = new List<ElementState>();
        }
        public GameState(int width, int length)
        {
            FieldWidth = width;
            FieldLength = length;
            ElementsState = new List<ElementState>();
        }

    }
}
