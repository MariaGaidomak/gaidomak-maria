﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Game.DTOs;

namespace Game
{
    public interface IMode
    {
        List<Level> GetLevels();
        void SaveState(GameState gameState);
        GameState RestoreState();
        void SaveResult(GameResult result);
        List<GameResult> GetGameStatistics();
    }
}
