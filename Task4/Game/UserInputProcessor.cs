﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game
{
    public class UserInputProcessor
    {
        public Action CallSaveState;
        public Action CallRestoreState;
        public Action CallShowGameResult;
        public ConsoleKeyInfo ReadKey()
        {
            ConsoleKeyInfo info = Console.ReadKey();
            switch (info.Key)
            {
                case ConsoleKey.LeftArrow:
                case ConsoleKey.RightArrow:
                case ConsoleKey.UpArrow:
                case ConsoleKey.DownArrow:
                    return info;
                case ConsoleKey.S:
                    CallSaveState();
                    break;
                case ConsoleKey.R:
                    CallRestoreState();
                    break;
                case ConsoleKey.L:
                    CallShowGameResult();
                    break;
            }
            return ReadKey();
        }
    }
}
