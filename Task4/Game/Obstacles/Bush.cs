﻿using System;
using Game.DTOs;


namespace Game
{
    public class Bush : PlayingElement, IImpactOnHelth, ISurmountabilityBlock
    {
        public Bush(int x, int y)
            : base(x, y)
        {
            Name = "Bush";
        }
        public Bush(ElementState state)
           : base(state)
        {
            Name = "Bush";
        }

        public int ImpactOnHelth { get; set; } = -5;
        public bool SurmountabilityBlock (PlayingElement element)
        {
            if(element is Player)
            {
                return true;
            }
            return false;
        }
        public override void Draw()
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.Write("B");
        }
        public override Types GetElementType()
        {
            return Types.Bush;
        }

    }
}
