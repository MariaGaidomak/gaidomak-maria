﻿using System;
using System.IO;
using Game.DTOs;
using System.Collections.Generic;

namespace Game
{
    public class Engine
    {
        private PlayingField _playingField;
        private Random _rnd = new Random();
        private FactoryElement _factory;
        private UserInputProcessor _inputProcessor;
        private IMode _mode;
        private List<Level> _levels;
        private int _index = 0;
        private int _stepsQuantity;
        private string _playerName;

        public Engine(IMode mode, string name)
        {
            _mode = mode;
            _levels = _mode.GetLevels();
            _inputProcessor = new UserInputProcessor();
            _inputProcessor.CallSaveState = SaveState;
            _inputProcessor.CallRestoreState = RestoreState;
            _inputProcessor.CallShowGameResult = ShowGameResult;
            _playerName = name;
        }
        private void RestoreState()
        {
            GameState gameState = _mode.RestoreState();
            if (gameState == null)
            {
                Console.WriteLine("You have no saved game states.");
                return;
            }
            _playingField = new PlayingField(gameState, _inputProcessor);

            _factory = new FactoryElement(_playingField);
            foreach (var item in gameState.ElementsState)
            {
                PlayingElement element = _factory.CreateElement(item);
                _playingField.AddElement(element);
            }
            Draw();
            _index = _levels.FindIndex(x => x.Levelid == gameState.LevelId);
        }
        public void SaveState()
        {
            GameState gameState = _playingField.GetState();
            gameState.LevelId = _levels[_index].Levelid;
            _mode.SaveState(gameState);
        }
        private void GeneratePlayingField()
        {
            _playingField = new PlayingField(_levels[_index].Width, _levels[_index].Height, _inputProcessor);
            _factory = new FactoryElement(_playingField);
            foreach (var item in _levels[_index].Elements)
            {
                for (int i = 0; i < item.PlayingElementQuantity; i++)
                {
                    PlayingElement element = _factory.CreateElement(item.PlayingElementType);
                    _playingField.AddElement(element);
                }
            }
        }
        private void LevelUp()
        {
            GameResult gameResult = new GameResult
            {
                GameDate = DateTime.Now,
                PlayerName = _playerName,
                StepsQuantity = _stepsQuantity,
                LevelId = _levels[_index].Levelid
            };
            _mode.SaveResult(gameResult);
            if (_index == _levels.Count - 1)
            {
                Console.WriteLine("Congratulations! You won!");
            }
            else
            {
                Console.WriteLine("Congratulations! You have reached the next level!");
                Console.ReadKey();
                Console.Clear();
                _index += 1;
                Run();
            }
        }
        public void Run()
        {
            GeneratePlayingField();
            do
            {
                Draw();
                if (!_playingField.HasBonuses())
                {
                    LevelUp();
                    return;
                }
                foreach (var actor in _playingField.GetActors())
                {
                    actor.DoStep();
                }
                _stepsQuantity++;
            } while (_playingField.Player.IsAlive());
            Console.SetCursorPosition(0, _playingField.FieldНeight + 3);
            Console.WriteLine("Game over.               ");
            Console.ReadLine();

        }
        public void ShowGameResult()
        {
            List<GameResult> results = _mode.GetGameStatistics();
            Console.Clear();
            foreach (var item in results)
            {
                Console.WriteLine($"Level: {item.LevelNumber}");
                Console.WriteLine($"Player name: {item.PlayerName}");
                Console.WriteLine($"Steps quantity: {item.StepsQuantity}");
                Console.WriteLine($"Game Date: {item.GameDate}");
                Console.WriteLine();
            }
        }

        private void Draw()
        {
            Console.ForegroundColor = ConsoleColor.White;
            for (int i = 0; i <= _playingField.FieldWidth + 1; i++)
            {
                Console.SetCursorPosition(i, 0);
                Console.Write("-");
                Console.SetCursorPosition(i, _playingField.FieldНeight + 1);
                Console.Write("-");
            }
            Console.SetCursorPosition(0, _playingField.FieldНeight + 3);
            Console.WriteLine();
            Console.SetCursorPosition(0, _playingField.FieldНeight + 3);
            Console.Write($"Player health = {_playingField.Player.Health}     ");
            for (int i = 0; i <= _playingField.FieldНeight + 1; i++)
            {
                Console.SetCursorPosition(0, i);
                Console.Write("|");
                Console.SetCursorPosition(_playingField.FieldWidth + 1, i);
                Console.Write("|");
            }
            for (int i = 0; i < _playingField.FieldWidth; i++)
            {

                for (int j = 0; j < _playingField.FieldНeight; j++)
                {
                    Console.SetCursorPosition(i + 1, j + 1);
                    PlayingElement element = _playingField.GetElement(i, j);
                    if (element == null)
                    {
                        Console.Write(" ");
                    }
                    else
                    {
                        element.Draw();
                    }
                }
            }
            Console.SetCursorPosition(_playingField.Player.CoordinateX + 1, _playingField.Player.CoordinateY + 1);
            _playingField.Player.Draw();
            Console.ForegroundColor = ConsoleColor.White;
            Console.SetCursorPosition(0, _playingField.FieldНeight + 3);
        }
    }
}

