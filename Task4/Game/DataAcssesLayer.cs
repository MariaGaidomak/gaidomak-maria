﻿using Microsoft.Data.SqlClient;
using System.Data;
using System.Collections.Generic;
using System;
using Game.DTOs;
using System.Globalization;

namespace Game
{
    public class DataAcssesLayer : IMode
    {
        private string _connectionString;
        public DataAcssesLayer(string connectionString)
        {
            _connectionString = connectionString;
        }

        public List<Level> GetLevels()
        {
            List<Level> levels = new List<Level>();
            Level level = null;

            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                SqlCommand command = connection.CreateCommand();
                command.CommandText = "select Type, Quantity, LevelNumber, FieldWidth, FieldHeight, LevelId from LevelData inner join Level on LevelId = Level.Id order by LevelNumber";
                command.CommandType = CommandType.Text;

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    int type = reader.GetByte("Type");
                    int quantity = reader.GetInt32("Quantity");
                    int levelNumber = reader.GetByte("LevelNumber");

                    if (level == null || levelNumber != level.LevelNumber)
                    {
                        level = new Level();
                        level.LevelNumber = levelNumber;
                        int width = reader.GetInt32("FieldWidth");
                        int height = reader.GetInt32("FieldHeight");
                        int levelId = reader.GetInt32("LevelId");
                        level.Width = width;
                        level.Height = height;
                        level.Levelid = levelId;

                        levels.Add(level);
                    }
                    level.Elements.Add(new PlayingElementSettings((Types)type, quantity));
                }
            }

            return levels;
        }

        public GameState RestoreState()
        {
            GameState gameState = new GameState();
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();

                command.CommandText = "select top 1 Id, LevelId from State order by LastSaveDate desc";
                SqlDataReader reader = command.ExecuteReader();
                if (!reader.Read())
                {
                    return null;
                }
                int stateId = reader.GetInt32("Id");
                int levelId = reader.GetInt32("LevelId");
                SqlParameter idParam = new("@StateId", stateId);
                SqlParameter levelIdParam = new("@LevelId", levelId);
                command.Parameters.Add(idParam);
                command.Parameters.Add(levelIdParam);
                reader.Close();

                command.CommandText = "select Id, FieldWidth, FieldHeight from Level where Id = @LevelId";
                reader = command.ExecuteReader();
                while (reader.Read())
                {
                    gameState.LevelId = reader.GetInt32("Id");
                    gameState.FieldWidth = reader.GetInt32("FieldWidth");
                    gameState.FieldLength = reader.GetInt32("FieldHeight");
                }
                reader.Close();

                command.CommandText = "select X, Y, Health, NumberOfPassesThroughObstacles from Player where StateId = @StateId";
                reader = command.ExecuteReader();
                while (reader.Read())
                {
                    ElementState element = new(Types.Player, reader.GetInt32("X"), reader.GetInt32("Y"));
                    element.Health = reader.GetByte("Health");
                    element.NumberOfPassesThroughObstacles = reader.GetByte("NumberOfPassesThroughObstacles");
                    gameState.Player = element;
                }
                reader.Close();

                command.CommandText = "select Type, X, Y, ImpactOnHealth, NumberOfPassesThroughObstacles from Monsters where StateId = @StateId";
                reader = command.ExecuteReader();
                while (reader.Read())
                {
                    int type = reader.GetByte("Type");
                    int x = reader.GetInt32("X");
                    int y = reader.GetInt32("Y");
                    int impactOnHealth = reader.GetInt16("ImpactOnHealth");
                    int numberOfPassesThroughObstacles = reader.GetByte("NumberOfPassesThroughObstacles");
                    ElementState element = new((Types)type, x, y);
                    element.ImpactOnHelth = impactOnHealth;
                    element.NumberOfPassesThroughObstacles = numberOfPassesThroughObstacles;
                    gameState.ElementsState.Add(element);
                }
                reader.Close();

                command.CommandText = "select Type, X, Y from Obstacles where StateId = @StateId";
                reader = command.ExecuteReader();
                while (reader.Read())
                {
                    int type = reader.GetByte("Type");
                    int x = reader.GetInt32("X");
                    int y = reader.GetInt32("Y");
                    ElementState element = new((Types)type, x, y);
                    gameState.ElementsState.Add(element);
                }
                reader.Close();

                command.CommandText = "select Type, X, Y from Bonuses where StateId = @StateId";
                reader = command.ExecuteReader();
                while (reader.Read())
                {
                    int type = reader.GetByte("Type");
                    int x = reader.GetInt32("X");
                    int y = reader.GetInt32("Y");
                    ElementState element = new((Types)type, x, y);
                    gameState.ElementsState.Add(element);
                }
            }
            return gameState;
        }

        public void SaveResult(GameResult result)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                SqlCommand command = connection.CreateCommand();
                command.Transaction = transaction;
                SqlParameter levelId = new("@LevelId", result.LevelId);
                SqlParameter playerName = new("@PlayerName", result.PlayerName);
                SqlParameter stepsQuantity = new("@StepsQuantity", result.StepsQuantity);
                SqlParameter gameDate = new("@GameDate", result.GameDate);
                command.Parameters.Add(playerName);
                command.Parameters.Add(stepsQuantity);
                command.Parameters.Add(gameDate);
                command.Parameters.Add(levelId);
                command.CommandText = "select count(*) from GameResult where LevelId = @LevelId";
                object resultQuantity = command.ExecuteScalar();
                if ((int)resultQuantity < 10)
                {
                    command.CommandText = "insert into GameResult (PlayerName, StepsQuantity, GameDate, LevelId) values (@PlayerName, @StepsQuantity, @GameDate, @LevelId)";
                    command.ExecuteNonQuery();
                }
                else
                {
                    command.CommandText = "select top 1 StepsQuantity from GameResult where LevelId = @LevelId order by StepsQuantity desc";
                    object steps = command.ExecuteScalar();
                    if ((int)steps > result.StepsQuantity)
                    {
                        return;
                    }
                    SqlParameter stepsQua = new SqlParameter("@StepsQua", (int)steps);
                    command.Parameters.Add(stepsQua);
                    command.CommandText = "delete from GameResult where StepsQuantity =  @StepsQua;" +
                        "insert into GameResult (PlayerName, StepsQuantity, GameDate, LevelId) values (@PlayerName, @StepsQuantity, @GameDate, @LevelId)";
                    command.ExecuteNonQuery();
                }
                transaction.Commit();
            }
        }

        public void SaveState(GameState gameState)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                SqlCommand command = connection.CreateCommand();
                command.Transaction = transaction;
                SqlParameter lastSaveDateParam = new("@LastSaveDate", DateTime.Now);
                SqlParameter levelIDParam = new("@LevelID", gameState.LevelId);
                command.Parameters.Add(lastSaveDateParam);
                command.Parameters.Add(levelIDParam);
                command.CommandText = "insert into State (LastSaveDate, LevelId) values (@LastSaveDate, @LevelId);" +
                                      "select scope_identity()";
                object stateId = command.ExecuteScalar();
                command.Parameters.Clear();
                foreach (var item in gameState.ElementsState)
                {
                    if (item.Type == Types.Bear || item.Type == Types.Wolf || item.Type == Types.Fox)
                    {
                        SqlParameter typeParam = new("@Type", item.Type);
                        SqlParameter xParam = new("@X", item.X);
                        SqlParameter yParam = new("@Y", item.Y);
                        SqlParameter impactOnHealthParam = new("@ImpactOnHealth", item.ImpactOnHelth);
                        SqlParameter numberOfPassesThroughObstaclesParam = new("@NumberOfPassesThroughObstacles", item.NumberOfPassesThroughObstacles);
                        SqlParameter stateIdParam = new("@StateId", stateId);
                        command.Parameters.Add(typeParam);
                        command.Parameters.Add(xParam);
                        command.Parameters.Add(yParam);
                        command.Parameters.Add(impactOnHealthParam);
                        command.Parameters.Add(numberOfPassesThroughObstaclesParam);
                        command.Parameters.Add(stateIdParam);
                        command.CommandText = $"insert into Monsters (Type, X, Y, ImpactOnHealth, NumberOfPassesThroughObstacles, StateId) values (@Type, @X, @Y, @ImpactOnHealth, @NumberOfPassesThroughObstacles, @StateId)";
                        command.ExecuteNonQuery();
                        command.Parameters.Clear();
                    }
                    else if (item.Type == Types.Bush || item.Type == Types.Wall || item.Type == Types.Stone)
                    {
                        SqlParameter typeParam = new("@Type", item.Type);
                        SqlParameter xParam = new("@X", item.X);
                        SqlParameter yParam = new("@Y", item.Y);
                        SqlParameter stateIdParam = new("@StateId", stateId);
                        command.Parameters.Add(typeParam);
                        command.Parameters.Add(xParam);
                        command.Parameters.Add(yParam);
                        command.Parameters.Add(stateIdParam);
                        command.CommandText = $"insert into Obstacles (Type, X, Y, StateId) values (@Type, @X, @Y, @StateId)";
                        command.ExecuteNonQuery();
                        command.Parameters.Clear();
                    }
                    else
                    {
                        SqlParameter typeParam = new("@Type", item.Type);
                        SqlParameter xParam = new("@X", item.X);
                        SqlParameter yParam = new("@Y", item.Y);
                        SqlParameter stateIdParam = new("@StateId", stateId);
                        command.Parameters.Add(typeParam);
                        command.Parameters.Add(xParam);
                        command.Parameters.Add(yParam);
                        command.Parameters.Add(stateIdParam);
                        command.CommandText = $"insert into Bonuses (Type, X, Y, StateId) values (@Type, @X, @Y, @StateId)";
                        command.ExecuteNonQuery();
                        command.Parameters.Clear();
                    }
                }
                SqlParameter xPlayerParam = new("@X", gameState.Player.X);
                SqlParameter yPlayerParam = new("@Y", gameState.Player.Y);
                SqlParameter HealthPlayerParam = new("@Health", gameState.Player.Health);
                SqlParameter numberOfPassesThroughObstaclesPlayerParam = new("@NumberOfPassesThroughObstacles", gameState.Player.NumberOfPassesThroughObstacles);
                SqlParameter stateIdPlayerParam = new("@StateId", stateId);
                command.Parameters.Add(xPlayerParam);
                command.Parameters.Add(yPlayerParam);
                command.Parameters.Add(HealthPlayerParam);
                command.Parameters.Add(numberOfPassesThroughObstaclesPlayerParam);
                command.Parameters.Add(stateIdPlayerParam);
                command.CommandText = $"insert into Player (X, Y, Health, NumberOfPassesThroughObstacles, StateId) values (@X, @Y, @Health, @NumberOfPassesThroughObstacles, @StateId)";
                command.ExecuteNonQuery();
                transaction.Commit();
                command.Parameters.Clear();
            }
        }
        public List<GameResult> GetGameStatistics()
        {
            List<GameResult> results = new List<GameResult>();
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();
                command.CommandText = "select PlayerName, StepsQuantity, GameDate, LevelNumber from GameResult inner join Level on LevelId = Level.Id order by LevelNumber, StepsQuantity";
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    string playerName = reader.GetString("PlayerName");
                    int stepsQuantity = reader.GetInt32("StepsQuantity");
                    DateTime gameDate = reader.GetDateTime("GameDate");
                    int levelNumber = reader.GetByte("LevelNumber");
                    results.Add(new GameResult
                    {
                        GameDate = gameDate,
                        PlayerName = playerName,
                        StepsQuantity = stepsQuantity,
                        LevelNumber = levelNumber
                    });
                }            
            }
            return results;
        }
    }
}
