﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Text.Json;
using Game.DTOs;

namespace Game
{
    public class LocalMode : IMode
    {
        public GameState RestoreState()
        {
            if (!File.Exists("State.json"))
            {
                return null;
            }
            string objectSerialized;
            using (StreamReader reader = new StreamReader("State.json"))
            {
                objectSerialized = reader.ReadLine();
            }
            object objectDeserialize = JsonSerializer.Deserialize(objectSerialized, typeof(GameState));
            GameState gameStateDeserialize = (GameState)objectDeserialize;
            return gameStateDeserialize;
        }
        public void SaveState(GameState gameState)
        {
            string objectSerialized = JsonSerializer.Serialize(gameState);
            using (StreamWriter writer = new StreamWriter("State.json"))
            {
                writer.WriteLine(objectSerialized);
            }
        }
        public void SaveResult(GameResult result)
        {
            Console.WriteLine("This function is not supported in this mode.");
        }
        public List<Level> GetLevels()
        {
            List<Level> levels = new List<Level>();
            Level level = new Level();
            level.LevelNumber = 1;
            level.Levelid = 1;
            level.Width = 10;
            level.Height = 10;
            level.Elements.Add(new PlayingElementSettings(Types.Wolf, 1));
            level.Elements.Add(new PlayingElementSettings(Types.Bear, 1));
            level.Elements.Add(new PlayingElementSettings(Types.Fox, 1));
            level.Elements.Add(new PlayingElementSettings(Types.Bush, 5));
            level.Elements.Add(new PlayingElementSettings(Types.Stone, 5));
            level.Elements.Add(new PlayingElementSettings(Types.Wall, 5));
            level.Elements.Add(new PlayingElementSettings(Types.Apple, 2));
            level.Elements.Add(new PlayingElementSettings(Types.Blueberry, 2));
            level.Elements.Add(new PlayingElementSettings(Types.Pineapple, 2));
            level.Elements.Add(new PlayingElementSettings(Types.Raspberries, 2));
            levels.Add(level);

            level = new Level();
            level.LevelNumber = 2;
            level.Levelid = 2;
            level.Width = 15;
            level.Height = 15;
            level.Elements.Add(new PlayingElementSettings(Types.Wolf, 3));
            level.Elements.Add(new PlayingElementSettings(Types.Bear, 3));
            level.Elements.Add(new PlayingElementSettings(Types.Fox, 3));
            level.Elements.Add(new PlayingElementSettings(Types.Bush, 10));
            level.Elements.Add(new PlayingElementSettings(Types.Stone, 10));
            level.Elements.Add(new PlayingElementSettings(Types.Wall, 10));
            level.Elements.Add(new PlayingElementSettings(Types.Apple, 8));
            level.Elements.Add(new PlayingElementSettings(Types.Blueberry, 8));
            level.Elements.Add(new PlayingElementSettings(Types.Pineapple, 8));
            level.Elements.Add(new PlayingElementSettings(Types.Raspberries, 8));
            levels.Add(level);

            level = new Level();
            level.LevelNumber = 3;
            level.Levelid = 3;
            level.Width = 20;
            level.Height = 20;
            level.Elements.Add(new PlayingElementSettings(Types.Wolf, 4));
            level.Elements.Add(new PlayingElementSettings(Types.Bear, 4));
            level.Elements.Add(new PlayingElementSettings(Types.Fox, 4));
            level.Elements.Add(new PlayingElementSettings(Types.Bush, 12));
            level.Elements.Add(new PlayingElementSettings(Types.Stone, 12));
            level.Elements.Add(new PlayingElementSettings(Types.Wall, 12));
            level.Elements.Add(new PlayingElementSettings(Types.Apple, 10));
            level.Elements.Add(new PlayingElementSettings(Types.Blueberry, 10));
            level.Elements.Add(new PlayingElementSettings(Types.Pineapple, 10));
            level.Elements.Add(new PlayingElementSettings(Types.Raspberries, 10));
            levels.Add(level);

            return levels;
        }
        public List<GameResult> GetGameStatistics()
        {
            Console.WriteLine("This function is not supported in this mode.");
            return null;
        }
    }
}
