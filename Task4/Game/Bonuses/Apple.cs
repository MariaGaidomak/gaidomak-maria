﻿using System;
using Game.DTOs;


namespace Game
{
    public class Apple : PlayingElement, IImpactOnHelth, IBonus
    {
        public Apple(int x, int y)
            : base(x, y)
        {
            Name = "Apple";
        }
        public Apple(ElementState state)
          : base(state)
        {
            Name = "Apple";
        }
        public int ImpactOnHelth { get; set; } = 10;
        public override void Draw()
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write("A");
        }
        public override Types GetElementType()
        {
            return Types.Apple;
        }

    }
}
