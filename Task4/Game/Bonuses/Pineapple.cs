﻿using System;
using Game.DTOs;


namespace Game
{
    public class Pineapple : PlayingElement, IImpactOnHelth, IBonus
    {
        public Pineapple(int x, int y)
            : base(x, y)
        {
            Name = "Pineapple";
        }
        public Pineapple(ElementState state)
           : base(state)
        {
            Name = "Pineapple";
        }
        public int ImpactOnHelth { get; set; } = 15;
        public override void Draw()
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write("P");
        }
        public override Types GetElementType()
        {
            return Types.Pineapple;
        }

    }
}
