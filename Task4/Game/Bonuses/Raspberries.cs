﻿using System;
using Game.DTOs;


namespace Game
{
    public class Raspberries : PlayingElement, IImpactOnHelth, IBonus
    {
        public Raspberries(int x, int y)
            : base(x, y)
        {
            Name = "Raspberries";
        }
        public Raspberries(ElementState state)
           : base(state)
        {
            Name = "Raspberries";
        }

        public int ImpactOnHelth { get; set; } = 10;
        public override void Draw()
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write("R");
        }
        public override Types GetElementType()
        {
            return Types.Raspberries;
        }
    }
}
