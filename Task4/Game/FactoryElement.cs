﻿using System;
using Game.DTOs;

namespace Game
{
    public class FactoryElement
    {
        private PlayingField _playingField;
        Random _rnd = new Random();
        public FactoryElement(PlayingField playingField)
        {
            _playingField = playingField;
        }
        private void GenerateCoordinate(out int x, out int y)
        {
            x = _rnd.Next(_playingField.FieldWidth - 1);
            y = _rnd.Next(_playingField.FieldНeight - 1);
            if (_playingField.GetElement(x, y) != null)
            {
                GenerateCoordinate(out x, out y);
            }
        }
        public PlayingElement CreateElement(Types type)
        {
            int x;
            int y;
            GenerateCoordinate(out x, out y);

            if (type == Types.Bear)
            {
                return new Bear(_playingField, x, y);
            }
            else if (type == Types.Wolf)
            {
                return new Wolf(_playingField, x, y);
            }
            else if (type == Types.Fox)
            {
                return new Fox(_playingField, x, y);
            }
            else if (type == Types.Bush)
            {
                return new Bush(x, y);
            }
            else if (type == Types.Stone)
            {
                return new Stone(x, y);
            }
            else if (type == Types.Wall)
            {
                return new Wall(x, y);
            }
            else if (type == Types.Apple)
            {
                return new Apple(x, y);
            }
            else if (type == Types.Blueberry)
            {
                return new Blueberry(x, y);
            }
            else if (type == Types.Pineapple)
            {
                return new Pineapple(x, y);
            }
            else if (type == Types.Raspberries)
            {
                return new Raspberries(x, y);
            }
            else if (type == Types.Hamster)
            {
                return new Hamster(_playingField, x, y);
            }
            else if (type == Types.Hare)
            {
                return new Hare(_playingField, x, y);
            }
            throw new Exception("There is no way to create a game item like this.");
        }
        public PlayingElement CreateElement(ElementState state)
        {
            Types type = state.Type;
            if (type == Types.Bear)
            {
                return new Bear(_playingField, state);
            }
            else if (type == Types.Wolf)
            {
                return new Wolf(_playingField, state);
            }
            else if (type == Types.Fox)
            {
                return new Fox(_playingField, state);
            }
            else if (type == Types.Bush)
            {
                return new Bush(state);
            }
            else if (type == Types.Stone)
            {
                return new Stone(state);
            }
            else if (type == Types.Wall)
            {
                return new Wall(state);
            }
            else if (type == Types.Apple)
            {
                return new Apple(state);
            }
            else if (type == Types.Blueberry)
            {
                return new Blueberry(state);
            }
            else if (type == Types.Pineapple)
            {
                return new Pineapple(state);
            }
            else if (type == Types.Raspberries)
            {
                return new Raspberries(state);
            }
            else if (type == Types.Hamster)
            {
                return new Hamster(_playingField, state);
            }
            else if (type == Types.Hare)
            {
                return new Hare(_playingField, state);
            }
            throw new Exception("There is no way to create a game item like this.");
        }
    }
}
