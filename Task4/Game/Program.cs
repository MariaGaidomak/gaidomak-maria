﻿using System;
using Game.DTOs;


namespace Game
{
    class Program
    {
        static public void GetInsruction()
        {
            Console.WriteLine(
            $"Keys for the command:{Environment.NewLine}" +
            $"l - game leaderboard statistics; {Environment.NewLine}" +
            $"r - restoring the state of the game; {Environment.NewLine}" +
            $"s - key for saving.{Environment.NewLine}");
        }
        static public void SelectMode(string userInput, string name)
        {
            if (userInput == "lg")
            {
                IMode mode = new LocalMode();
                Engine engine = new Engine(mode, name);
                engine.Run();
            }
            else if (userInput == "sg")
            {
                const string connectionString = @"Data Source=localhost;Initial Catalog=GameDB;Integrated Security=True";
                IMode mode = new DataAcssesLayer(connectionString);
                Engine engine = new Engine(mode, name);
                engine.Run();
            }
            else
            {
                Console.WriteLine("Wrong key.");
            }
        }
        static void Main(string[] args)
        {
            GetInsruction();
            Console.WriteLine("Enter player name:");
            string name = Console.ReadLine();
            while (String.IsNullOrWhiteSpace(name))
            {
                Console.WriteLine("Invalid name! Try again.");
                name = Console.ReadLine();
            }
            Console.WriteLine("Please enter the key to select the game mode:  lg - \"local game\" mode, sg - \"server game\" mode");
            string userInput = Console.ReadLine();
            Console.Clear();
            SelectMode(userInput, name);
        }
    }
}
