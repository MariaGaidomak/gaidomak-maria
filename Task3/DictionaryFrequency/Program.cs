﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Globalization;
namespace DictionaryFrequency
{
    class Program
    {
        private static Dictionary<char, int> _symbolsdDictionary = new Dictionary<char, int>();
        private static Dictionary<string, int> _wordsDictionary = new Dictionary<string, int>();
        public static void WriteInLogFile<TKey>(string nameLogFile, Dictionary<TKey, int> dictionaryFrequency)
        {
            using (StreamWriter writer = new StreamWriter(nameLogFile))
            {
                foreach (var item in dictionaryFrequency)
                {
                    writer.WriteLine($"{item.Key} - {item.Value}");
                }
            }
        }
        public static void ProcessFiles(IEnumerable<String> files)
        {
            foreach (string file in files)
            {
                IEnumerable<string> lines = File.ReadLines(file);
                foreach (var line in lines)
                {
                    FillInSymbolsDictionary(line);
                    FillInWordsDictionary(line);
                }

            }
        }
        public static Dictionary<char, int> FillInSymbolsDictionary(string line)
        {
            for (int i = 0; i < line.Length; i++)
            {
                    if (!_symbolsdDictionary.Keys.Contains(Char.ToLower(line[i])))
                    {
                        _symbolsdDictionary.Add(Char.ToLower(line[i]), 1);
                    }
                    else
                    {
                        _symbolsdDictionary[Char.ToLower(line[i])]++;
                    }
            }
            return _symbolsdDictionary;
        }

        public static void FillInWordsDictionary(string line)
        {
            char[] symbols = new char[10] {' ', '"', ',', '.', ';', '!', '?', '-', ':', '\''};
            string[] words = line.Split(symbols, StringSplitOptions.RemoveEmptyEntries);
            for (int i = 0; i < words.Length; i++)
            {
                if (!_wordsDictionary.Keys.Contains(words[i].ToLower()))
                {
                    _wordsDictionary.Add(words[i].ToLower(), 1);
                }
                else
                {
                    _wordsDictionary[words[i].ToLower()]++;
                }
            }

        }
        static void Main(string[] args)
        {
            if (args.Length == 0)
            {
                Console.WriteLine("For the application to work correctly, you need a parameter - folder name.");
                return;
            }
            if (!Directory.Exists(args[0]))
            {
                Console.WriteLine("Wrong directory name.");
                return;
            }
            string folderName = args[0];
            string symbolsFile ="symbols.txt";
            string wordsFile = "words.txt";
            IEnumerable<String> files = Directory.EnumerateFiles(folderName, "*.txt", SearchOption.AllDirectories);
            ProcessFiles(files);
            WriteInLogFile<char>(symbolsFile, _symbolsdDictionary);
            WriteInLogFile<string>(wordsFile, _wordsDictionary);
        }
    }
}
