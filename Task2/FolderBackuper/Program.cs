﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Linq;

namespace FolderBackuper
{
    class Program
    {
        private static string _folderName;
        private static string _backFolderName;
        private static string _logName;
        private static List<FileCopyInfo> _filesCopyInfos;
        public static void GetInstruction()
        {
            Console.WriteLine($"The sequence of passing arguments when starting the application:{Environment.NewLine}" +
                $"1. Folder name; {Environment.NewLine}" +
                $"2. Key to set the mode {Environment.NewLine}" +
                $"3. Date to rollback in the format: \"day.month.year hour:minute:second\". {Environment.NewLine}{Environment.NewLine}" +
                $"Keys for the command:{Environment.NewLine}" +
                $"-w - key to call the watch mode; {Environment.NewLine}" +
                $"-r - key to call rollback mode; {Environment.NewLine}" +
                "-h or -help or no arguments - key for instructions.");
        }
        private static void Watch()
        {
            Directory.CreateDirectory(_backFolderName);
            DirectoryInfo info = new DirectoryInfo(_folderName);
            FileInfo[] fileInfos = info.GetFiles();
            _filesCopyInfos = new List<FileCopyInfo>();
            foreach (FileInfo item in fileInfos)
            {
                string id = Guid.NewGuid().ToString();
                string fileName = Path.Combine(_backFolderName, id);
                FileCopyInfo fileCopyInfo = new FileCopyInfo(id, item.FullName);
                _filesCopyInfos.Add(fileCopyInfo);
                File.Copy(item.FullName, fileName);
            }
            WriteLog();
            FileSystemWatcher watcher = new FileSystemWatcher(_folderName);
            watcher.EnableRaisingEvents = true;
            watcher.NotifyFilter = NotifyFilters.CreationTime
                                 | NotifyFilters.FileName
                                 | NotifyFilters.LastWrite
                                 | NotifyFilters.LastAccess;
            watcher.Changed += OnChanged;
            watcher.Created += OnCreated;
            watcher.Deleted += OnDeleted;
            watcher.Renamed += OnRenamed;
        }
        private static void WriteLog()
        {
            using (StreamWriter writer = new StreamWriter(_logName, true))
            {
                DateTime dateTime = DateTime.Now;
                writer.WriteLine(dateTime);
                foreach (FileCopyInfo item in _filesCopyInfos)
                {
                    writer.WriteLine(item.ID);
                    writer.WriteLine(item.FullName);
                }
            }
        }
        private static void OnChanged(object sender, FileSystemEventArgs e)
        {
            if (e.FullPath == _backFolderName)
            {
                return;
            }
            string id = Guid.NewGuid().ToString();
            string fileName = Path.Combine(_backFolderName, id);
            File.Copy(e.FullPath, fileName);
            FileCopyInfo fileInfoToUpdate = _filesCopyInfos.Find(file => file.FullName == e.FullPath);
            fileInfoToUpdate.ID = id;
            WriteLog();
        }
        private static void OnCreated(object sender, FileSystemEventArgs e)
        {
            string id = Guid.NewGuid().ToString();
            _filesCopyInfos.Add(new FileCopyInfo(id, e.FullPath));
            File.Copy(e.FullPath, Path.Combine(_backFolderName, id));
            WriteLog();
        }
        private static void OnDeleted(object sender, FileSystemEventArgs e)
        {
            FileCopyInfo fileInfoToUpdate = _filesCopyInfos.Find(file => file.FullName == e.FullPath);
            _filesCopyInfos.Remove(fileInfoToUpdate);
            WriteLog();
        }
        private static void OnRenamed(object sender, RenamedEventArgs e)
        {
            FileCopyInfo fileInfoToUpdate = _filesCopyInfos.Find(file => file.FullName == e.OldFullPath);
            fileInfoToUpdate.FullName = e.FullPath;
            WriteLog();
        }
        private static void Rollback(DateTime rollbackDate)
        {
            string[] lines = File.ReadAllLines(_logName);
            List<FolderCopyInfo> folderCopyInfos = FolderCopyInfo.GetFolderCopyInfos(lines);
            FolderCopyInfo stateToRollback = folderCopyInfos
                .FindAll(element => element.TimeStamp < rollbackDate)
                .OrderByDescending(x => x.TimeStamp)
                .FirstOrDefault();
            if(stateToRollback != null)
            {
                DirectoryInfo info = new DirectoryInfo(_folderName);
                FileInfo[] fileInfos = info.GetFiles();
                foreach (FileInfo item in fileInfos)
                {
                    item.Delete();
                }
                foreach (FileCopyInfo item in stateToRollback.Files)
                {
                    File.Copy(Path.Combine(_backFolderName, item.ID), item.FullName);
                }
            }
            else
            {
                Console.WriteLine("The rollback date does not match the earliest backup date.");
            }
        }
        static void Main(string[] args)
        {
            if (args.Length == 0 || args.Contains("-h") || args.Contains("-help"))
            {
                GetInstruction();
                return;
            }

            string folderName = args[0];
            if (!Directory.Exists(folderName))
            {
                Console.WriteLine("Wrong directory name.");
                return;
            }
            string mode = args[1];
            _folderName = folderName;
            _backFolderName = Path.Combine(folderName, ".Backup");
            _logName = Path.Combine(_backFolderName, "LogBackup.log");    
            if (mode == "-w")
            {
                Watch();
                Console.ReadKey();
                return;
            }
            else if(mode == "-r")
            {
                DateTime rollbackDate;
                if (DateTime.TryParse(args[2],out rollbackDate))
                {
                    rollbackDate = DateTime.Parse(args[2]);
                    Rollback(rollbackDate);
                }
                Console.WriteLine("Invalid date format. Read the instructions.");
                Console.WriteLine();
                GetInstruction();

            }
            else
            {
                Console.WriteLine("Incorrect mode selected. Read the instructions.");
                Console.WriteLine();
                GetInstruction();
            }
        }
    }
}
