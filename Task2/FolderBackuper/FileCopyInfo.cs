﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FolderBackuper
{
    public class FileCopyInfo
    {
        public string ID { get; set; }
        public string FullName { get; set; }
        public FileCopyInfo(string id, string fullName)
        {
            ID = id;
            FullName = fullName;
        }
    }
}
