﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace searcher
{
    public class FileResultInfo
    {
        public string Name { get; set; }
        public List<ItemInfo> Items { get; set; }
        public FileResultInfo(string name, List<ItemInfo> items)
        {
            Name = name;
            Items = items;
        }
    }
}
